
import 'package:flutter/material.dart';
import 'package:news_app/src/models/news_models.dart';
import 'package:news_app/src/theme/theme.dart';

class NewsList extends StatelessWidget {

  final List<Article> news;

  const NewsList(this.news);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.news.length,
      itemBuilder: (BuildContext context, int index) {
        return _NewsItem(newsItem: this.news[index], index: index,);
      },
    );
  }
}

class _NewsItem extends StatelessWidget {

  final Article newsItem;
  final int index;

  const _NewsItem({
    @required this.newsItem,
    @required this.index
  });


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _CardTopBar(newsItem, index),
        _CardTitle(newsItem),
        _CardImage(newsItem),
        _CardBody(newsItem),
        _CardButtons(),
        SizedBox(height: 10.0),
        Divider()
      ],
    );
  }
}

class _CardButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        RawMaterialButton(
          fillColor: myTheme.accentColor,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          onPressed: () {},
          child: Icon(Icons.star_border),
        ),
        SizedBox(width: 10.0),
        RawMaterialButton(
          fillColor: Colors.blue,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          onPressed: () {},
          child: Icon(Icons.more),
        )
      ],
    );
  }
}

class _CardBody extends StatelessWidget {

  final Article newsItem;

  const _CardBody(this.newsItem);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Text((newsItem.description != null) ? newsItem.description : '')
    );
  }
}

class _CardImage extends StatelessWidget {

  final Article newsItem;

  const _CardImage(this.newsItem);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: ClipRRect(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0), bottomRight: Radius.circular(50.0)),
        child: Container(
          child: (newsItem.urlToImage != null)
            ? FadeInImage(
                placeholder: AssetImage('assets/giphy.gif'),
                image: NetworkImage(newsItem.urlToImage)
              )
            : Image(image: AssetImage('assets/no-image.png'))
        ),
      ),
    );
  }
}

class _CardTitle extends StatelessWidget {

  final Article newsItem;

  const _CardTitle(this.newsItem);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: Text(newsItem.title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
    );
  }
}

class _CardTopBar extends StatelessWidget {

  final Article newsItem;
  final int index;

  const _CardTopBar(this.newsItem, this.index);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      margin: EdgeInsets.only(bottom: 10.0),
      child: Row(
        children: <Widget>[
          Text('${index+1}. ', style: TextStyle(color: myTheme.accentColor),),
          Text('${newsItem.source.name}. ',),
        ],
      ),
    );
  }
}